package com.jaguarlabs.brtest.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by mxrampage on 27/02/16.
 */
public class NetStatChangeReceiver extends BroadcastReceiver{
    NetStatChecker netStatChecker;

    @Override
    public void onReceive(Context context, Intent intent) {
        netStatChecker = new NetStatChecker(context);
        if (netStatChecker.isConnected()){
            Toast.makeText(context,"Internet available",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context,"Internet not available",Toast.LENGTH_SHORT).show();
        }
    }
}
