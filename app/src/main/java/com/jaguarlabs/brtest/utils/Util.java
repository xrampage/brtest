package com.jaguarlabs.brtest.utils;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by mxrampage on 27/02/16.
 */
public class Util {
    public static String STORELOGO = "storeLogoURL";
    public static String STORENAME = "name";
    public static String STOREPHONE = "phone";
    public static String STOREADDRESS = "address";
    public static String STORECITY = "city";
    public static String STORESTATE = "state";
    public static String STOREZIP = "zipcode";
    public static String STORELAT = "latitude";
    public static String STORELON = "longitude";
    public static DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
            .considerExifParams(true)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();

}
