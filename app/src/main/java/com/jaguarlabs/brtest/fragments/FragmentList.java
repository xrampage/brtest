package com.jaguarlabs.brtest.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.jaguarlabs.brtest.R;
import com.jaguarlabs.brtest.adapters.StoreDataAdapter;
import com.jaguarlabs.brtest.models.ModelStores;
import com.jaguarlabs.brtest.rest.Model.DataPetitionResponse;
import com.jaguarlabs.brtest.rest.Request.GetDataPetition;
import com.jaguarlabs.brtest.rest.RestService;
import com.jaguarlabs.brtest.rest.SpiceHelper;
import com.jaguarlabs.brtest.utils.NetStatChecker;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mxrampage on 27/02/16.
 */
public class FragmentList extends Fragment {
    //List of objects used by the class
    View vMain;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ModelStores> storesArrayList;
    private SpiceHelper<DataPetitionResponse> spiceDataPetition;
    private SpiceManager spiceManager = new SpiceManager(RestService.class);

    //Initialize spiceManager object, used by Robospice to establish connections
    //Check if spiceDataPetition is initialized and has a pending request, if
    //both are true then execute that pending request
    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(getActivity());
        if (spiceDataPetition != null && spiceDataPetition.isRequestPending()){
            spiceDataPetition.executePendingRequest();
        }
    }

    //Stop spiceManager object, as it will no longer be used by the class
    @Override
    public void onStop() {
        super.onStop();
        if (spiceManager.isStarted()){
            spiceManager.shouldStop();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        vMain = inflater.inflate(R.layout.fragment_list, container, false);
        initializers();
        //Set the necessary parameters needed by the RecyclerView in order to populate it with an adapter
        //(added later when user receives a Success in the petition
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        //Check if fragment is loaded for first time
        if (savedInstanceState == null){
            //Establish a refresh listener to the swipeRefreshLayout object to determine what it should do when it
            //detects a Refresh action (getStoreData), changed the color schematic on the Refresh indicator
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipeRefreshLayout.setColorSchemeResources(R.color.blue, R.color.red, R.color.yellow, R.color.green);
                    getStoreData();
                }
            });
            //Create a new SpiceHelper<DataPetitionResponse> instance, established what should happen
            //on a successful or failed response
            spiceDataPetition = new SpiceHelper<DataPetitionResponse>(spiceManager, "dataCache", DataPetitionResponse.class) {
                @Override
                protected void onFail(SpiceException exception) {
                    super.onFail(exception);
                    if (swipeRefreshLayout.isRefreshing()){
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }

                @Override
                protected void onSuccess(DataPetitionResponse dataPetitionResponse) {
                    super.onSuccess(dataPetitionResponse);
                    if (dataPetitionResponse != null){
                        if (dataPetitionResponse.getStoresList().size() != 0){
                            List<ModelStores> storesList = ModelStores.listAll(ModelStores.class);
                            if (storesList.size() != 0){
                                ModelStores.deleteAll(ModelStores.class);
                            }
                            storesArrayList = new ArrayList<>();
                            for (ModelStores modelStores : dataPetitionResponse.getStoresList()){
                                storesArrayList.add(modelStores);
                                ModelStores stores = new ModelStores();
                                stores.setAddress(modelStores.getAddress());
                                stores.setCity(modelStores.getCity());
                                stores.setLatitude(modelStores.getLatitude());
                                stores.setLongitude(modelStores.getLongitude());
                                stores.setName(modelStores.getName());
                                stores.setPhone(modelStores.getPhone());
                                stores.setState(modelStores.getState());
                                stores.setStoreID(modelStores.getStoreID());
                                stores.setStoreLogoURL(modelStores.getStoreLogoURL());
                                stores.setZipcode(modelStores.getZipcode());
                                stores.save();
                            }
                            StoreDataAdapter storeDataAdapter = new StoreDataAdapter(storesArrayList, getActivity().getSupportFragmentManager());
                            recyclerView.setAdapter(storeDataAdapter);
                            if (swipeRefreshLayout.isRefreshing()){
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        } else {
                            showSnackbar("No data was received.");
                        }
                    }
                }
            };
            getStoreData();
        } else {
            //If fragment was already loaded try and populate adapter using cache, if it's not possible
            //display the "No cache found" warning
            final List<ModelStores> storesList = ModelStores.listAll(ModelStores.class);
            if (storesList.size() == 0){
                showSnackbar("No cache found, try again when there's an active data connection");
            } else {
                ArrayList<ModelStores> modelStoresArrayList = new ArrayList<>();
                for (ModelStores stores : storesList) {
                    ModelStores modelStores = new ModelStores();
                    modelStores.setAddress(stores.getAddress());
                    modelStores.setCity(stores.getCity());
                    modelStores.setLatitude(stores.getLatitude());
                    modelStores.setLongitude(stores.getLongitude());
                    modelStores.setName(stores.getName());
                    modelStores.setPhone(stores.getPhone());
                    modelStores.setState(stores.getState());
                    modelStores.setStoreID(stores.getStoreID());
                    modelStores.setStoreLogoURL(stores.getStoreLogoURL());
                    modelStores.setZipcode(stores.getZipcode());
                    modelStoresArrayList.add(modelStores);
                }
                StoreDataAdapter storeDataAdapter = new StoreDataAdapter(modelStoresArrayList, getActivity().getSupportFragmentManager());
                recyclerView.setAdapter(storeDataAdapter);
            }
        }
        return vMain;
    }

    //Initialize the UI objects
    private void initializers() {
        recyclerView = (RecyclerView) vMain.findViewById(R.id.rv_list);
        swipeRefreshLayout = (SwipeRefreshLayout) vMain.findViewById(R.id.srl_list_container);
    }

    //Method that starts the connection to the server to pull the json
    private void getStoreData() {
        NetStatChecker netStatChecker = new NetStatChecker(getActivity());
        //If theres an active data or internet connection fetch data from server
        if (netStatChecker.isConnected()){
            if (!spiceDataPetition.isRequestPending()){
                GetDataPetition getDataPetition = new GetDataPetition();
                spiceDataPetition.executeRequest(getDataPetition);
            }
        } else {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            getCacheData();
        }
    }

    //Method to invoke material design widget snackbar to display messages
    //based on the String passed to the method.
    private void showSnackbar(String message){
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(Color.parseColor("#FF4081"));
        snackbar.show();
    }

    private void getCacheData(){
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", "No internet connection, checking cache", true);
        //If there's no active data or internet connection check database to see if there's info in it
        final List<ModelStores> storesList = ModelStores.listAll(ModelStores.class);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                //If database is empty show a snackbar indicating there's no cache
                if (storesList.size() == 0) {
                    showSnackbar("No cache found, try again when there's Internet access");
                } else {
                    //If database has records show a snackbar indicating that user is viewing records from cache
                    showSnackbar("Loading data from cache");
                    ArrayList<ModelStores> modelStoresArrayList = new ArrayList<>();
                    for (ModelStores stores : storesList) {
                        ModelStores modelStores = new ModelStores();
                        modelStores.setAddress(stores.getAddress());
                        modelStores.setCity(stores.getCity());
                        modelStores.setLatitude(stores.getLatitude());
                        modelStores.setLongitude(stores.getLongitude());
                        modelStores.setName(stores.getName());
                        modelStores.setPhone(stores.getPhone());
                        modelStores.setState(stores.getState());
                        modelStores.setStoreID(stores.getStoreID());
                        modelStores.setStoreLogoURL(stores.getStoreLogoURL());
                        modelStores.setZipcode(stores.getZipcode());
                        modelStoresArrayList.add(modelStores);
                    }
                    StoreDataAdapter storeDataAdapter = new StoreDataAdapter(modelStoresArrayList, getActivity().getSupportFragmentManager());
                    recyclerView.setAdapter(storeDataAdapter);
                }
            }
        }, 3000);
    }

}
