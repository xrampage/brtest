package com.jaguarlabs.brtest.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jaguarlabs.brtest.R;
import com.jaguarlabs.brtest.utils.Util;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DecimalFormat;

/**
 * Created by mxrampage on 27/02/16.
 */
public class FragmentDataDisplay extends Fragment {
    //List of objects used by the class
    View vMain;
    ImageView ivDetailStoreLogo;
    private MapView mapView;
    ImageLoader imageLoader = ImageLoader.getInstance();
    TextView tvDetailName, tvDetailPhone, tvDetailAddress, tvDetailZipCode;
    String detailLogoURL, detailName, detailPhone, detailAddress;
    double detailZipCode, detailLatitude, detailLongitude;
    GoogleMap googleMap;

    @Override
    public void onResume(){
        super.onResume();
        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vMain = inflater.inflate(R.layout.fragment_display, container, false);
        //Check if Google Play Services is available, else the app will crash as it can't load map objects
        if (isGooglePlayServicesAvailable(getActivity())){
            //Initialize map object, call methods necessary to move camera to a position
            mapView = (MapView) vMain.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);
            googleMap = mapView.getMap();
            MapsInitializer.initialize(getActivity());
        } else {
            showSnackbar("No Google Play Services installed on the device");
        }
        initializers();
        //Create a new Bundle to receive arguments from FragmentList fragment, if it is not null
        //then set the values on their respective TextViews
        Bundle args = getArguments();
        if (args != null) {
            detailLogoURL = args.getString(Util.STORELOGO);
            detailName = args.getString(Util.STORENAME);
            detailPhone = args.getString(Util.STOREPHONE);
            detailAddress = args.getString(Util.STOREADDRESS) + ", " + args.getString(Util.STORECITY) + ", " + args.getString(Util.STORESTATE);
            detailZipCode = args.getDouble(Util.STOREZIP);
            detailLatitude = args.getDouble(Util.STORELAT);
            detailLongitude = args.getDouble(Util.STORELON);
            imageLoader.displayImage(detailLogoURL, ivDetailStoreLogo, Util.defaultOptions);
            tvDetailName.setText(detailName);
            tvDetailPhone.setText(detailPhone);
            tvDetailPhone.setTextColor(Color.rgb(0, 0, 238));
            tvDetailAddress.setText(detailAddress);
            DecimalFormat decimalFormat = new DecimalFormat("#");
            tvDetailZipCode.setText(String.valueOf(decimalFormat.format(detailZipCode)));
            //Check GoogleMap object, if it's not null add a marker on it
            if (googleMap != null){
                googleMap.addMarker(new MarkerOptions().position(new LatLng(detailLatitude, detailLongitude)));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(detailLatitude, detailLongitude), 16.0f));
            }
            //Establish a ClickListener to handle an Intent which will launche the Dialer with the
            //phone number indicated in the TextView
            tvDetailPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + detailPhone));
                    getActivity().startActivity(intent);
                }
            });
        }
        return vMain;
    }

    //Initialize the UI objects
    private void initializers() {
        ivDetailStoreLogo = (ImageView) vMain.findViewById(R.id.iv_detail_store_logo);
        tvDetailName = (TextView) vMain.findViewById(R.id.tv_detail_name);
        tvDetailPhone = (TextView) vMain.findViewById(R.id.tv_detail_phone);
        tvDetailAddress = (TextView) vMain.findViewById(R.id.tv_detail_address);
        tvDetailZipCode = (TextView) vMain.findViewById(R.id.tv_detail_zipcode);
    }

    public boolean isGooglePlayServicesAvailable(Context context){
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        return  resultCode == ConnectionResult.SUCCESS;
    }

    //Method to invoke material design widget snackbar to display messages
    //based on the String passed to the method.
    private void showSnackbar(String message){
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(Color.parseColor("#FF4081"));
        snackbar.show();
    }

}