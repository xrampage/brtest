package com.jaguarlabs.brtest;

import android.app.FragmentManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jaguarlabs.brtest.fragments.FragmentList;
import com.jaguarlabs.brtest.utils.Util;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends AppCompatActivity {
    FragmentList fragmentList;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        //Iinitiate Imageloader from Android Universal Image Loader gallery
        if (!ImageLoader.getInstance().isInited()){
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                    .defaultDisplayImageOptions(Util.defaultOptions)
                    .build();
            ImageLoader.getInstance().init(config);
        }
        if (savedInstanceState == null){
            fragmentList = new FragmentList();
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, fragmentList, "list")
                    .addToBackStack(null).commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        //getSupportFragmentManager().putFragment(outState, "list", fragmentList);
    }

    //Handle what actions to take when the user presses the back button
    @Override
    public void onBackPressed() {
        String currentTag = getSupportFragmentManager().findFragmentById(R.id.fl_container).getTag();
        if (currentTag.equals("display")){
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
}
