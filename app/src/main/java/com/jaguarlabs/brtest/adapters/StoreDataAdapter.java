package com.jaguarlabs.brtest.adapters;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jaguarlabs.brtest.R;
import com.jaguarlabs.brtest.fragments.FragmentDataDisplay;
import com.jaguarlabs.brtest.models.ModelStores;
import com.jaguarlabs.brtest.utils.Util;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * Created by mxrampage on 27/02/16.
 */
public class StoreDataAdapter extends RecyclerView.Adapter<StoreDataAdapter.StoreDataViewHolder> {
    ImageLoader imageLoader = ImageLoader.getInstance();
    List<ModelStores> storesList;
    FragmentManager fragmentManager;

    public StoreDataAdapter(List<ModelStores> storesList, FragmentManager fragmentManager){
        this.storesList = storesList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public StoreDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inner_list_layout, parent, false);
        return new StoreDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final StoreDataViewHolder holder, int position) {
        final ModelStores stores = storesList.get(position);
        imageLoader.displayImage(stores.getStoreLogoURL(),holder.ivInnerStoreLogo, Util.defaultOptions, new SimpleImageLoadingListener(){
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                super.onLoadingStarted(imageUri, view);
                holder.pbInnerLoader.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                holder.ivInnerStoreLogo.setImageBitmap(loadedImage);
                holder.pbInnerLoader.setVisibility(View.GONE);
            }
        });
        holder.tvInnerAddress.setText(stores.getAddress());
        holder.tvInnerPhoneNumber.setText(stores.getPhone());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString(Util.STORELOGO,stores.getStoreLogoURL());
                args.putString(Util.STORENAME,stores.getName());
                args.putString(Util.STOREPHONE,stores.getPhone());
                args.putString(Util.STOREADDRESS, stores.getAddress());
                args.putString(Util.STORECITY, stores.getCity());
                args.putString(Util.STORESTATE, stores.getState());
                args.putDouble(Util.STOREZIP, stores.getZipcode());
                args.putDouble(Util.STORELAT, stores.getLatitude());
                args.putDouble(Util.STORELON, stores.getLongitude());
                FragmentDataDisplay fragmentDataDisplay = new FragmentDataDisplay();
                fragmentDataDisplay.setArguments(args);
                fragmentManager.beginTransaction().add(R.id.fl_container, fragmentDataDisplay, "display")
                        .addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return storesList.size();
    }

    public class StoreDataViewHolder extends RecyclerView.ViewHolder {
        protected ImageView ivInnerStoreLogo;
        protected TextView tvInnerPhoneNumber;
        protected TextView tvInnerAddress;
        protected ProgressBar pbInnerLoader;

        public StoreDataViewHolder(View itemView) {
            super(itemView);
            ivInnerStoreLogo = (ImageView) itemView.findViewById(R.id.iv_inner_store_logo);
            tvInnerPhoneNumber = (TextView) itemView.findViewById(R.id.tv_inner_phone_number);
            tvInnerAddress = (TextView) itemView.findViewById(R.id.tv_inner_address);
            pbInnerLoader = (ProgressBar) itemView.findViewById(R.id.pb_inner_loader);
        }
    }
}
