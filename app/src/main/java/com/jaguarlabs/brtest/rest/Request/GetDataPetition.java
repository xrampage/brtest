package com.jaguarlabs.brtest.rest.Request;

import com.jaguarlabs.brtest.rest.Model.DataPetitionResponse;
import com.jaguarlabs.brtest.rest.WebServices.WSDataPetition;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.octo.android.robospice.retry.RetryPolicy;

/**
 * Created by mxrampage on 27/02/16.
 */
public class GetDataPetition extends RetrofitSpiceRequest<DataPetitionResponse, WSDataPetition>{
    public GetDataPetition() {
        super(DataPetitionResponse.class, WSDataPetition.class);
    }

    @Override
    public DataPetitionResponse loadDataFromNetwork() throws Exception {
        return getService().dataPetitionResponse();
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(0, 0, 1);
    }

}
