package com.jaguarlabs.brtest.rest.Model;

import com.google.gson.annotations.SerializedName;
import com.jaguarlabs.brtest.models.ModelStores;

import java.util.List;

/**
 * Created by mxrampage on 27/02/16.
 */
public class DataPetitionResponse {
    @SerializedName("stores")
    private List<ModelStores> storesList;

    public List<ModelStores> getStoresList() {
        return storesList;
    }

    public void setStoresList(List<ModelStores> storesList) {
        this.storesList = storesList;
    }

}
