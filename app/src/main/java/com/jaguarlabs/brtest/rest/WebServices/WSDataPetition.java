package com.jaguarlabs.brtest.rest.WebServices;

import com.jaguarlabs.brtest.rest.Model.DataPetitionResponse;

import retrofit.http.GET;

/**
 * Created by mxrampage on 27/02/16.
 */
public interface WSDataPetition {
    @GET("/stores.json")
    DataPetitionResponse dataPetitionResponse();

}
