package com.jaguarlabs.brtest.rest;

import android.app.Application;
import android.util.Log;

import com.jaguarlabs.brtest.rest.Model.DataPetitionResponse;
import com.jaguarlabs.brtest.rest.WebServices.WSDataPetition;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import java.io.IOException;
import java.net.HttpURLConnection;

import retrofit.RestAdapter;
import retrofit.client.Request;
import retrofit.client.UrlConnectionClient;

/**
 * Created by mxrampage on 25/09/15.
 */
public class RestService extends RetrofitGsonSpiceService {
    private static final String BASE_URL = "http://sandbox.bottlerocketapps.com/BR_Android_CodingExam_2015_Server";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(WSDataPetition.class);
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();
        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setLog(new RestAdapter.Log() {

            @Override
            public void log(String message) {
                Log.i("My tag", message);
            }
        });

        builder.setClient(new UrlConnectionClient() {
            @Override
            protected HttpURLConnection openConnection(Request request) throws IOException {
                HttpURLConnection connection = super.openConnection(request);
                connection.setConnectTimeout(10 * 1000);
                connection.setReadTimeout(10 * 1000);
                return connection;
            }
        });
        return builder;
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager manager = new CacheManager();
        GsonObjectPersister<DataPetitionResponse> petitionResponseGsonObjectPersister = new GsonObjectPersister<>(application, DataPetitionResponse.class);
        manager.addPersister(petitionResponseGsonObjectPersister);
        return manager;
    }

    @Override
    public int getThreadCount() {
        return 2;
    }

}
